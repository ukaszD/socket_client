#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <stdlib.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <string.h>
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#define PORT "6666"
#define CLIENT_ADDRESS "10.0.0.12"
#define BUFFER_SIZE 1024

int __cdecl main()
{
	WSADATA wsaData;
	SOCKET new_socket = INVALID_SOCKET;
	struct addrinfo *result = NULL;
	struct addrinfo	*ptr = NULL;
	struct addrinfo	hints;

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		perror("!!!WSAStartup error!!!");
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	if (getaddrinfo(CLIENT_ADDRESS, PORT, &hints, &result) != 0) {
		perror("!!!Get address info error!!!");
		WSACleanup();
		return 1;
	}

	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		new_socket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (new_socket == INVALID_SOCKET) {
			perror("!!!Socket error!!!\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}
		if (connect(new_socket, ptr->ai_addr, (int)ptr->ai_addrlen) == SOCKET_ERROR) {
			closesocket(new_socket);
			new_socket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);
	if (new_socket == INVALID_SOCKET) {
		perror("!!!Connect error!!!\n");
		WSACleanup();
		return 1;
	}

	char buffer[BUFFER_SIZE] = "--connect_request--";
	if (send(new_socket, buffer, (int)strlen(buffer), 0) == SOCKET_ERROR) {
		perror("!!!Send error!!!\n", WSAGetLastError());
		closesocket(new_socket);
		WSACleanup();
		return;
	}
	else {
		printf("<connect request sent>\n<waiting...>\n");

		do {
			ZeroMemory(&buffer, BUFFER_SIZE);

			if (recv(new_socket, buffer, BUFFER_SIZE, 0) == SOCKET_ERROR) {
				perror("!!!Receive error!!!\n", WSAGetLastError());
				closesocket(new_socket);
				WSACleanup();
				return;
			}

			if (strncmp("exit", buffer, 2) == 0) {
				perror("!!!Server disconnected!!!\n");
				break;
			}

			printf("[Server]: %s\n", buffer);
			printf("> ");
			ZeroMemory(&buffer, BUFFER_SIZE);
			scanf("%s", buffer);

			if (send(new_socket, buffer, (int)strlen(buffer), 0) == SOCKET_ERROR) {
				printf("!!!Send disconnected!!!\n", WSAGetLastError());
				closesocket(new_socket);
				WSACleanup();
				return;
			}
		} while (strncmp("exit", buffer, 2) != 0);
	}

	closesocket(new_socket);
	WSACleanup();
	return 0;
}